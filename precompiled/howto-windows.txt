I don't have time to edit Makefile, so:

1. Install OCaml and CamlP4. camlp4o is named camlp4o.opt.exe on Windows, but it's not working as expected

2. Pre-optimize parser.ml using: 
camlp4o.opt.exe parser.ml -o parseropt.ml
mv parseropt.ml parser.ml

3. Change CC_PARSER_CMD to
CC_PARSER_CMD = $(COMPILER) parser.ml

4. Install flex through cygwin